const generateSummary = callback => fetch("data.json")
    .then(response => response.json())
    .then(callback);

const generateSummaryButton = document.querySelector("#generateSummaryButton");
const reportTable = document.getElementById("reportTable");

generateSummaryButton.addEventListener("click", () => {
    generateSummary(data => {
        fillTableWithData(data, reportTable);
    });
});

function fillTableWithData(data, table) {
    while (table.firstChild) {
        table.removeChild(table.firstChild);
    }

    const marks = data.marks;
    for (let i = 0; i < marks.length; i++) {
        const newRow = document.createElement("tr");
        const nameCell = document.createElement("td");
        const subjectCell = document.createElement("td");
        const gradeCell = document.createElement("td");

        nameCell.textContent = `Name: ${marks[i].student}`;
        subjectCell.textContent = `Subject: ${marks[i].subject}`;
        gradeCell.textContent = `Grade: ${marks[i].grade}`;

        nameCell.classList.add("name");
        subjectCell.classList.add("subject");
        gradeCell.classList.add("grade");

        newRow.appendChild(nameCell);
        newRow.appendChild(subjectCell);
        newRow.appendChild(gradeCell);
        table.appendChild(newRow);
    }
}
